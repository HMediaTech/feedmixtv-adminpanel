/**
 * Main application controller
 *
 * You can use this controller for your whole app if it is small
 * or you can have separate controllers for each logical section
 *
 */
;(function() {

  angular
    .module('boilerplate')
    .controller('FeedMixController', FeedMixController);

  FeedMixController.$inject = ['$scope', '$http'];

  function FeedMixController($scope, $http) {

    angular.element(document).ready(function () {
      //flip on show

      setInterval(function(){
       $http.get(url)
            .then(function(posts){
          console.log(posts);
          $scope.posts = posts.data;
        });
      }, 1000);
      

      setTimeout(function(){
        $('.card').toggleClass('flipped');
      },500);

      $scope.addSource=function(dataSource, elementId){
        
        var sourceValue = jQuery(elementId).val();
        var source="";

        if(dataSource=="accounts"){
           source=$scope.posts.accounts;
        }
        else if(dataSource=="subreddits"){
           source=$scope.posts.reddit.subreddits;
        }
        else if(dataSource=="queries"){
           source=$scope.posts.reddit.queries;
        }
        else if(dataSource=="rss"){
           source=$scope.posts.rss;
        }
        var sourceArr = eval(source);
        var valueIsContained = false;
         
        //checking if new element exists on the server
        for(var i=0; i<sourceArr.length; i++){
          valueIsContained = valueIsContained || (sourceArr[i] == sourceValue);
        }
        
        if (!valueIsContained) {
          // console.log('add sourceValue', dataSource, elementId);
          $http({
            url:url+'/add',
            method:"POST",
            data:{
                'key': dataSource,
                'value': sourceValue
            }
          })
          $(elementId).val('');
        }
      }
    

      $scope.deleteSource=function(key,value){
        var index = -1;
        var source="";   
        if(key=="accounts"){
          source=$scope.posts.accounts;
        }
        else if(key=="subreddits"){
          source=$scope.posts.reddit.subreddits;
        }
        else if(key=="queries"){
          source=$scope.posts.reddit.queries;
        }
        else if(key=="rss"){
          source=$scope.posts.rss;
        }
        else{
          console.log("could not find key!");
        }
          var sourceArr = eval(source);
          for( var i = 0; i < sourceArr.length; i++ ) {
            if( sourceArr[i] === value ) {
              index = i;
              break;
            }
          }
          if( index === -1 ) {
            alert( "Something gone wrong" );
          }
          
          //$scope.posts.reddit.subreddits.splice( index, 1 );
        
         $http({
           url:url+'/delete',
           method:"POST",
           data:{
               'key':key,
               'value':sourceArr[index]
           }
         })
      }

    });
        
    // 'controller as' syntax
    var self = this;

    //todo, get posts here

    var url = 'http://52.232.43.104:8080/feedmix/restservice/config';

  }


})();